PURPOSE of GA-main.cpp, SA-main.cpp and "hybrid (GA + SA)-main.cpp": Output results of testing on a single instance, into a file.
-----------------------------------------------------------------------------------------------
The "INSTANCES" directory contains instances by which parameters were set, and for which the implementations were tested.
The "INSTANCES-all" directory contains all AP, CAB and URAND instances.
-----------------------------------------------------------------------------------------------

Tag "ADJUST" marks parameters to adjust when using code on different types of instances. See science paper.
RECOMMENDATION: To find parts that should be adjusted faster, add Tag "ADJUST" to Todo Tree.

ADJUST:
In GA-main.cpp:
- parameter values in instance of struct PrametriGA.
Parameters: velicinaPopulacije,
		brojElitnihJedinki,
		velicinaTurnira,
		verovatnocaMutacije,
		MAX_VREME_GA
- choose which crossover type will be called (in 2 places).
  Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
- output file name

In SA-main.cpp:
- choose between simulirano_kaljenje_algoritam1() and simulirano_kaljenje_algoritam2().
- when using simulirano_kaljenje_algoritam1() choose which value for "zavesa" will be used. Change this in SA.cpp.
- parameter values in instance of struct  PrametriSA.
Parameters: MAX_VREME_SA,
		pocetnaTemperatura,
		parametarHladjenja,
		maxPonavljanjaNaIstojTemp
The last three parameters should only be set when simulirano_kaljenje_algoritam2() is used.
- output file name

In "hybrid (GA + SA)-main.cpp": Everything mentioned for SA as well as GA.

NOTE: Parameters could have been set beforehand which would make running the programs easier. However, more efficiency is achieved by setting different parameter values for different types/sizes of instances.

-----------------------------------------------------------------------------------------------

COMPILE:
1) g++ -o SA SA-main.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/SA.cpp includes/modFloydWarshallFuncs.cpp
2) g++ -o GA GA-main.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/GA.cpp includes/modFloydWarshallFuncs.cpp
3) g++ -o hybrid "hybrid (GA + SA)-main.cpp" includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/GA.cpp includes/SA.cpp includes/modFloydWarshallFuncs.cpp

RUN PROGRAMS:
1) ./SA INSTANCES/<instance folder name>/<instance file name>
example: ./SA INSTANCES/small/AP102.txt

2) ./GA INSTANCES/<instance folder name>/<instance file name>
example: ./GA INSTANCES/small/AP102.txt

3) ./hybrid INSTANCES/<instance folder name>/<instance file name>
example: ./hybrid INSTANCES/small/AP102.txt