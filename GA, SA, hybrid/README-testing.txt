PURPOSE of GA-tesitng.cpp, SA-tesitng.cpp and "hybrid (GA + SA)-tesitng.cpp": Output results of testing on instances specified in input file, into an output file.
-----------------------------------------------------------------------------------------------
When testing CHANGE:
- value of brojPokretanjaNadJednomInstancom, for small and medium instances I set it to 10, for large to 5
- parameter values
- destination of the input (instances) file
- destinations of the two output files in main
- the fourth argument of function test, which represents the number of instances of the same type (AP, CAB, URAND) from the input instances file
- ONLY FOR GA AND HYBRID: choose which crossover type will be called (in 2 places).
  Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
- ONLY FOR SA AND HYBRID: choose between simulirano_kaljenje_algoritam1() and simulirano_kaljenje_algoritam2().
  When using simulirano_kaljenje_algoritam1() choose which value for "zavesa" will be used. Change this in SA.cpp.
  NOTE: parameters pocetnaTemperatura, parametarHladjenja and maxPonavljanjaNaIstojTemp should only be set when simulirano_kaljenje_algoritam2() is used.

Tag "ADJUST" marks parameters to adjust when using code on different types of instances. See science paper.
RECOMMENDATION: To find parts that should be adjusted faster, add Tag "ADJUST" to Todo Tree.

NOTE: Parameters could have been set beforehand which would make running the programs easier. However, more efficiency is achieved by setting different parameter values for different types/sizes of instances.
---------------------------------------------------------------------------------------------------------------------------

The "INSTANCES" directory contains instances by which parameters were set, and for which the implementations were tested.
The "INSTANCES-all" directory contains all AP, CAB and URAND instances.

---------------------------------------------------------------------------------------------------------------------------

COMPILE:
1) g++ -o SA-testing SA-testing.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/SA.cpp includes/modFloydWarshallFuncs.cpp
2) g++ -o GA-testing GA-testing.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/GA.cpp includes/modFloydWarshallFuncs.cpp
3) g++ -o hybrid-testing "hybrid (GA + SA)-testing.cpp" includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp includes/SA.cpp includes/GA.cpp includes/modFloydWarshallFuncs.cpp

RUN PROGRAMS:
1) ./SA-testing

2) ./GA-testing

3) ./hybrid-testing

---------------------------------------------------------------------------------------------------------------------------

RUN PROGRAMS with parameters set according to science paper:
1) FOR SA:
  1.1) ./SA-testing-forSmall
  1.2) ./SA-testing-forMedium
  1.3) ./SA-testing-forLarge

2) FOR GA:
  2.1) ./GA-testing-forSmall
  2.2) ./GA-testing-forMedium
  2.3) ./GA-testing-forLarge

3) FOR HYBRID:
  3.1)./hybrid-testing-forSmall
  3.2)./hybrid-testing-forMedium
  3.3)./hybrid-testing-forLarge