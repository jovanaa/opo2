#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include "includes/SA.h"
#include "includes/graph.h"
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

int main(int argc, char** argv) {

	ParametriSA parametri;
	// ADJUST: Adjust parameter values.
	parametri.MAX_VREME_SA = 1000;	// Time in milliseconds.
	#if 0	// Set the following parameters only if Algorithm 2 is used for simulirano_kaljenje().
	parametri.pocetnaTemperatura = 43482.6461;
	parametri.parametarHladjenja = 0.4709;	
	parametri.maxPonavljanjaNaIstojTemp = 4097;
	#endif
	
	srand(time(NULL));

	unsigned instance;
	std::cout << "\nUnesite broj 1 ako se program pokrece nad CAB instancama, broj 2 ukoliko se pokrece nad AP instancama ili broj 3 ukoliko su u pitanju URAND instance: ";
	std::cin >> instance;
	std::cout << "\n";
	static_cast<tipInstance>(instance);

	Graf graf;	// Includes: brojCvorova, cene (cost matrix), p, alfa

	// Read instance file.
	if (instance == CAB) 
		graf = ucitajCAB(argv[1]);
	else if (instance == AP)
		graf = ucitajAP(argv[1]);
	else if (instance == URAND)
		graf = ucitajURAND(argv[1]);
	else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

	std::vector<int> habovi; // Set of hubs. In the GA this is used as the code of an individual.
	std::vector<int> haboviNajbolje;

	Stoperica stopericaSA;	// Start timer.

	habovi = inicijalizuj(graf);
	// ADJUST: Choose between simulirano_kaljenje_algoritam1() and simulirano_kaljenje_algoritam2().
	// In case simulirano_kaljenje_algoritam1() is used, there are 2 options for function "zavesa". See SA.cpp.
	double vrednostResenja = simulirano_kaljenje_algoritam1(std::cout,
															graf,
															habovi,
															haboviNajbolje,
															parametri,
															stopericaSA);

	// Output time spent, NOT including time for calculating allocation matrices.
	double protekloVremePreAB = stopericaSA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms \n";

	// Calculate allocation matrices.
	std::vector<std::vector<double> > dist;
	std::vector<std::vector<int> > A; // First allocation matrix.
	std::vector<std::vector<int> > B; // Second allocation matrix.
	inicijalizacija_pocetnih_matrica_alokacije(dist, 
											haboviNajbolje,
											graf,
											A,
											B);
	popuni_matrice_alokacije(dist,
							haboviNajbolje,
							graf,
							A,
							B);

	// Output time spent, including time for calculating allocation matrices.
	double protekloVreme = stopericaSA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme: " << protekloVreme << " ms \n";

	// Output results.
	std::ofstream outputFajl("AP102-rezultati.txt");	// ADJUST: Adjust output file name.
	ispisi_vrednost_resenja_i_habove(outputFajl, vrednostResenja, haboviNajbolje, graf.p);
	ispisi_matrice_alokacije(outputFajl, A , B, graf.brojCvorova);
	outputFajl.close();

	return 0;
}
