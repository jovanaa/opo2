#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include "includes/GA.h"
#include "includes/SA.h"
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

int main(int argc, char** argv) {

	ParametriGA parametriGA;
	// ADJUST: Adjust parameter values.
	parametriGA.velicinaPopulacije = 141;
    parametriGA.brojElitnihJedinki = 119;
    parametriGA.velicinaTurnira = 6;
    parametriGA.verovatnocaMutacije = 0.5611;
    parametriGA.MAX_VREME_GA = 1000;	// Time in milliseconds.

	ParametriSA parametriSA;
	// ADJUST: Adjust parameter values.
	parametriSA.MAX_ITERACIJA_SA = 467;
	#if 0	// Set the following parameters only if simulirano_kaljenje_algoritam2_hibrid() is used.
	parametriSA.pocetnaTemperatura = 43482.6461;
	parametriSA.parametarHladjenja = 0.4709;	
	parametriSA.maxPonavljanjaNaIstojTemp = 475;
	#endif
	
	srand(time(NULL));

	unsigned instance;
	std::cout << "\nUnesite broj 1 ako se program pokrece nad CAB instancama, broj 2 ukoliko se pokrece nad AP instancama ili broj 3 ukoliko su u pitanju URAND instance: ";
	std::cin >> instance;
	std::cout << "\n";
	static_cast<tipInstance>(instance);

	Graf graf;	// Includes: brojCvorova, cene (cost matrix), p, alfa

	// Read instance file.
	if (instance == CAB) 
		graf = ucitajCAB(argv[1]);
	else if (instance == AP)
		graf = ucitajAP(argv[1]);
	else if (instance == URAND)
		graf = ucitajURAND(argv[1]);
	else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}
	
	Stoperica meriUkupnoVreme;	// Start timer.

	// Generate initial generation.
	std::vector<Jedinka> populacija;
	std::vector<Jedinka> novaPopulacija;
	populacija.reserve(parametriGA.velicinaPopulacije);
	novaPopulacija.reserve(parametriGA.velicinaPopulacije);
	for (int i = 0; i < parametriGA.velicinaPopulacije; i++) {
		populacija.emplace_back(Jedinka(graf));
		novaPopulacija.emplace_back(Jedinka(graf));
	}
	std::sort(populacija.begin(), populacija.end(), uporedi);
	
	double najboljaVrednost = populacija[0].fitnes;
	std::cout << "\n\tTrenutna najbojla vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
	double protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
	std::cout << "\n\tVreme: " << protekloVremeGA << " ms \n";

	// ADJUST: Choose between simulirano_kaljenje_algoritam1_hibrid() and simulirano_kaljenje_algoritam2_hibrid().
	// In case simulirano_kaljenje_algoritam1_hibrid() is used, there are 2 options for function "zavesa". See SA.cpp.
	// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost.
	najboljaVrednost = simulirano_kaljenje_algoritam1_hibrid(std::cout,
																graf,
																populacija[0] /* jedinka */,
																parametriSA,
																parametriGA,
																meriUkupnoVreme /* stoperica */);	
	
	int neparanBrojJedinkiZaUkrstanjeIMutaciju = (parametriGA.velicinaPopulacije - parametriGA.brojElitnihJedinki) % 2;
	while (protekloVremeGA < parametriGA.MAX_VREME_GA) {

		for (int i = 0; i < parametriGA.brojElitnihJedinki; i++)
			novaPopulacija[i] = populacija[i];
		
		// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
		if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
			for (int i = parametriGA.brojElitnihJedinki; i < parametriGA.velicinaPopulacije-1; i+=2) {
				int i1 = selekcija(parametriGA, populacija);
				int i2 = selekcija(parametriGA, populacija);
				
				// ADJUST: Choose which crossover type will be called.
				// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
				uniformno_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(graf, parametriGA, novaPopulacija[i]);
				mutacija(graf, parametriGA, novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
			}

			mutacija(graf,
					parametriGA,
					novaPopulacija[parametriGA.velicinaPopulacije-1]);
			novaPopulacija[parametriGA.velicinaPopulacije-1].fitnes = novaPopulacija[parametriGA.velicinaPopulacije-1].fitnes_funkcija(graf);
		} else {	// Even number of individuals on which genetic operators should be applied.
			for (int i = parametriGA.brojElitnihJedinki; i < parametriGA.velicinaPopulacije; i+=2) {
				int i1 = selekcija(parametriGA, populacija);
				int i2 = selekcija(parametriGA, populacija);
				
				// ADJUST: Choose which crossover type will be called.
				// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
				uniformno_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(graf, parametriGA, novaPopulacija[i]);
				mutacija(graf, parametriGA, novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
				
			}
		}
		populacija = novaPopulacija;
		std::sort(populacija.begin(), populacija.end(), uporedi);

		if (populacija[0].fitnes < najboljaVrednost) {
			najboljaVrednost = populacija[0].fitnes;
			
			std::cout << "\tTrenutna najbojla vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
			double vremeTrazenjaNoveNajboljeVrednosti = meriUkupnoVreme.izracunaj_proteklo_vreme();
			std::cout << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		// ADJUST: Choose between simulirano_kaljenje_algoritam1_hibrid() and simulirano_kaljenje_algoritam2_hibrid().
		// In case simulirano_kaljenje_algoritam1_hibrid() is used, there are 2 options for function "zavesa". See SA.cpp.
		// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost.
		najboljaVrednost = simulirano_kaljenje_algoritam1_hibrid(std::cout,
																graf,
																populacija[0] /* jedinka */,
																parametriSA,
																parametriGA,
																meriUkupnoVreme /* stoperica */);
		
		protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
	}

	// Output time spent, NOT including time for calculating allocation matrices.
	double protekloVremePreAB = meriUkupnoVreme.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms \n";

	// Calculate allocation matrices.
	std::vector<std::vector<double> > dist;
	std::vector<std::vector<int> > A; // First allocation matrix.
	std::vector<std::vector<int> > B; // Second allocation matrix.
	inicijalizacija_pocetnih_matrica_alokacije(dist, 
												populacija[0].kod /* haboviNajbolje */,
												graf,
												A,
												B);
	popuni_matrice_alokacije(dist,
							populacija[0].kod /* habovi */,
							graf,
							A,
							B);

	// Output time spent, including time for calculating allocation matrices.
	double protekloVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme: " << protekloVreme << " ms \n";

	// Output results.
	std::ofstream outputFajl("AP102-rezultati.txt");	// ADJUST: Adjust output file name.
	ispisi_vrednost_resenja_i_habove(outputFajl,
									populacija[0].fitnes /* vrednostResenja */,
									populacija[0].kod /* habovi */,
									graf.p);
	ispisi_matrice_alokacije(outputFajl, A , B, graf.brojCvorova);
	outputFajl.close();
	
	return 0;
}
