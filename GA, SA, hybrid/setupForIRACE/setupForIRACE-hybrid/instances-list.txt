## This is an example of specifying instances with a file.

# Each line is an instance relative to trainInstancesDir
# (see scenario.txt.tmpl) and an optional sequence of instance-specific
# parameters that will be passed to target-runnerx when invoked on that
# instance.

# Empty lines and comments are ignored.

# Small instances
#AP/AP102.txt
#AP/AP103.txt
#AP/AP104.txt
#AP/AP105.txt
#AP/AP202.txt
#AP/AP203.txt
#AP/AP204.txt
#AP/AP205.txt
#CAB/CAB10.txt --brojCvorova 10 --p 2 --alfa 0.2
#CAB/CAB10.txt --brojCvorova 10 --p 2 --alfa 0.8
#CAB/CAB10.txt --brojCvorova 10 --p 4 --alfa 0.4
#CAB/CAB10.txt --brojCvorova 10 --p 4 --alfa 0.6
#CAB/CAB15.txt --brojCvorova 15 --p 2 --alfa 0.2
#CAB/CAB15.txt --brojCvorova 15 --p 3 --alfa 0.2
#CAB/CAB15.txt --brojCvorova 15 --p 4 --alfa 0.2
#CAB/CAB15.txt --brojCvorova 15 --p 4 --alfa 1.0
#CAB/CAB20.txt --brojCvorova 20 --p 3 --alfa 0.6
#CAB/CAB20.txt --brojCvorova 20 --p 3 --alfa 1.0
#CAB/CAB20.txt --brojCvorova 20 --p 4 --alfa 0.6
#CAB/CAB20.txt --brojCvorova 20 --p 4 --alfa 1.0

# Medium instances
#AP/AP252.txt
#AP/AP253.txt
#AP/AP255.txt
#AP/AP254.txt
#AP/AP2510.txt
#AP/AP402.txt
#AP/AP502.txt
#AP/AP503.txt
#AP/AP505.txt
#AP/AP5010.txt
#AP/AP1002.txt
#AP/AP1005.txt
#AP/AP10010.txt
#urand/tm100.02.txt
#urand/tm100.03.txt
#urand/tm100.04.txt
#urand/tm100.05.txt
#urand/tm100.10.txt
#urand/tm100.15.txt
#urand/tm100.20.txt

# Large instances
urand/tm200.03.txt
urand/tm200.05.txt
urand/tm200.10.txt
urand/tm200.15.txt
urand/tm200.20.txt
urand/tm300.03.txt
urand/tm300.05.txt
urand/tm300.10.txt
urand/tm300.15.txt
urand/tm300.20.txt
urand/tm400.03.txt
urand/tm400.05.txt
urand/tm400.10.txt
urand/tm400.15.txt
urand/tm400.20.txt
urand/tm1000.02.txt
urand/tm1000.03.txt
urand/tm1000.05.txt
urand/tm1000.10.txt
urand/tm1000.20.txt