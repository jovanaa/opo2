#pragma once

#include <iostream>
#include <chrono>

class Stoperica {
public:
	Stoperica();

	double izracunaj_proteklo_vreme();
	// void ispisi_proteklo_vreme(double trajanje_ms);

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> pocetniTrenutak;
};