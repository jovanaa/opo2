#include "modFloydWarshallFuncs.h"
#include <algorithm>
#include "typesOfInstances.h"
#include "inputFunctions.h"
#include "timer.h"
#include "allocationMatrices.h"
#include "outputFunctions.h"

constexpr int POZITIVNA_BESKONACNOST = 1e9;

// Time complexity: O(n^2*p), where n = graf.brojCvorova.
void inicijalizacija_pocetne_matrice(const Graf& graf,
                                    std::vector<int>& habovi,
                                    std::vector<std::vector<double> >& dist) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

	dist.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++){
		dist[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(habovi), std::end(habovi), i);
            auto nadjiJuHabovi = std::find(std::begin(habovi), std::end(habovi), j);

            if (nadjiIuHabovi == std::end(habovi) && nadjiJuHabovi == std::end(habovi))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(habovi) && nadjiJuHabovi != std::end(habovi))
                dist[i][j] = graf.alfa * graf.cene[i][j];
            else
                dist[i][j] = graf.cene[i][j];

			// If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            #if 0
            else if (nadjiIuHabovi == std::end(habovi))
                dist[i][j] = graf.delta * graf.cene[i][j];
            else
                dist[i][j] = graf.gama * graf.cene[i][j];
            #endif

		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = graf.brojCvorova
void pronadji_najkrace_puteve(const Graf& graf,
                                std::vector<int>& habovi,
                                std::vector<std::vector<double> >& dist) {

	for (int brojac = 0; brojac < graf.p; brojac++)
		for (int i = 0; i < graf.brojCvorova; i++)
			for (int j = 0; j < graf.brojCvorova; j++)
					dist[i][j] = std::min(dist[i][j], dist[i][habovi[brojac]] + dist[habovi[brojac]][j]);
}

// Time complexity: O(n^2), where n = graf.brojCvorova.
double pronadji_max_cenu_najkracih_puteva(const Graf& graf, std::vector<std::vector<double> >& dist) {
    double max = 0;

	for (int i = 0; i < graf.brojCvorova; i++)
		for (int j = 0; j < graf.brojCvorova; j++)
				max = std::max(max, dist[i][j]);

	return max;
}