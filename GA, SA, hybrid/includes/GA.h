#pragma once

#include <vector>
#include "graph.h"
#include "timer.h"

struct ParametriGA {
    int velicinaPopulacije;
    int brojElitnihJedinki;
    int velicinaTurnira;
    double verovatnocaMutacije;
    double MAX_VREME_GA;
};

class Jedinka {
public:
	// Set of hubs. In the SA implementation this is a vector called "habovi".
	std::vector<int> kod;
	double fitnes;
	
	Jedinka(const Graf& graf);
	
	// This function is the same as vrednost_resenja() in the SA implementation.
	double fitnes_funkcija(const Graf& graf) ;
};

int selekcija(const ParametriGA& parametri,
                std::vector<Jedinka>& populacija);
void jednopoziciono_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2);
void dvopoziciono_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2);
void uniformno_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2);
void mutacija(const Graf& graf,
                const ParametriGA& parametri,
                Jedinka& jedinka);
bool uporedi(const Jedinka& i1, const Jedinka& i2);