#include "GA.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include "modFloydWarshallFuncs.h"
#include "typesOfInstances.h"
#include "inputFunctions.h"
#include "timer.h"
#include "allocationMatrices.h"
#include "outputFunctions.h"

constexpr int POZITIVNA_BESKONACNOST = 1e9;

Jedinka::Jedinka(const Graf& graf) {
	kod.resize(graf.p);
	for (int j = 0; j < graf.p; j++) {
		kod[j] = std::rand() % graf.brojCvorova;
	}
		
	fitnes = fitnes_funkcija(graf);
}
	
double Jedinka::fitnes_funkcija(const Graf& graf) {
	std::vector<std::vector<double> > dist;

	inicijalizacija_pocetne_matrice(graf, kod /* habovi */, dist);
	pronadji_najkrace_puteve(graf, kod /* habovi */, dist);
	double max = pronadji_max_cenu_najkracih_puteva(graf, dist);

	return max;
};

int selekcija(const ParametriGA& parametri, std::vector<Jedinka>& populacija) {
	double min = POZITIVNA_BESKONACNOST;
	int pobednik = -1;
	
	for (int i = 0; i < parametri.velicinaTurnira; i++) {
		int k = std::rand() % parametri.velicinaPopulacije;
		
		if (populacija[k].fitnes < min) {
			min = populacija[k].fitnes;
			pobednik = k;
		}
	}
	
	return pobednik;
}

void jednopoziciono_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	int indeks = std::rand() % p;
	
	for (int i = 0; i < indeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = indeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}

void dvopoziciono_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	// Pick two random indexes.
	int prviIndeks = std::rand() % p;
	int drugiIndeks;
	do {
		drugiIndeks = std::rand() % p;
	} while (prviIndeks == drugiIndeks);

	// Store the picked values so that: prviIndeks < drugiIndeks.
	if (drugiIndeks < prviIndeks) {
		int pomocna = prviIndeks;
		prviIndeks = drugiIndeks;
		drugiIndeks = pomocna;
	}
	
	for (int i = 0; i < prviIndeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = prviIndeks; i < drugiIndeks; i++) {
		potomak1.kod[i] = roditelj2.kod[i];
		potomak2.kod[i] = roditelj1.kod[i];
	}

	for (int i = drugiIndeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}

void uniformno_ukrstanje(int p,
                Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	
	for (int i = 0; i < p; i++) {
		int roditeljZaGenPotomka1 = std::rand() % 2;

		if (roditeljZaGenPotomka1 == 1) {
			potomak1.kod[i] = roditelj1.kod[i];
			potomak2.kod[i] = roditelj2.kod[i];
		} else {
			potomak1.kod[i] = roditelj2.kod[i];
			potomak2.kod[i] = roditelj1.kod[i];
		}
	}
}

void mutacija(const Graf& graf,
                const ParametriGA& parametri,
                Jedinka& jedinka) {
	int k;
	
	for (int i = 0; i < graf.p; i++) {
		if (std::rand() % 100 > parametri.verovatnocaMutacije)
			continue;
			
		do {
			k = std::rand() % graf.brojCvorova;
		} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), k) != jedinka.kod.end());
		jedinka.kod[i] = k;
	}
}

bool uporedi(const Jedinka& i1, const Jedinka& i2) {
	return i1.fitnes < i2.fitnes;
}