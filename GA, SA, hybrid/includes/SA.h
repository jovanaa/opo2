#pragma once

#include <vector>
#include "graph.h"
#include "GA.h"
#include "timer.h"

struct ParametriSA {
	double MAX_VREME_SA;
    int MAX_ITERACIJA_SA;   // Only for hybrid.
	double pocetnaTemperatura;
	double parametarHladjenja;	
	double maxPonavljanjaNaIstojTemp;
};

std::vector<int> inicijalizuj(const Graf& graf);
double vrednost_resenja(const Graf& graf, std::vector<int>& habovi);
double simulirano_kaljenje_algoritam1(std::ostream& izlaz,
                                        const Graf& graf,
                                        std::vector<int>& habovi,
                                        std::vector<int>& haboviNajbolje,
                                        const ParametriSA& parametri,
                                        Stoperica& stoperica);
double simulirano_kaljenje_algoritam2(std::ostream& izlaz,
                                        const Graf& graf,
                                        std::vector<int>& habovi,
                                        std::vector<int>& haboviNajbolje,
                                        const ParametriSA& parametri,
                                        Stoperica& stoperica);
double simulirano_kaljenje_algoritam1_hibrid(std::ostream& izlaz,
												const Graf& graf,
												Jedinka& jedinka,
												const ParametriSA& parametriSA,
												const ParametriGA& parametriGA,
												Stoperica& stoperica);
double simulirano_kaljenje_algoritam2_hibrid(std::ostream& izlaz,
												const Graf& graf,
												Jedinka& jedinka,
												const ParametriSA& parametriSA,
												const ParametriGA& parametriGA,
												Stoperica& stoperica);                                                                                       
