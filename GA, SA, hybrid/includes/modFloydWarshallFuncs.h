#pragma once

#include <vector>
#include "graph.h"
#include "timer.h"

void inicijalizacija_pocetne_matrice(const Graf& graf,
                                        std::vector<int>& habovi,
                                        std::vector<std::vector<double> >& dist);
void pronadji_najkrace_puteve(const Graf& graf,
                                std::vector<int>& habovi,
                                std::vector<std::vector<double> >& dist);
double pronadji_max_cenu_najkracih_puteva(const Graf& graf, std::vector<std::vector<double> >& dist);
