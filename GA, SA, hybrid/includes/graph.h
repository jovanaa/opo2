#pragma once

#include <vector>

struct Graf {
    int brojCvorova;
	std::vector<std::vector<double> > cene;
	int p;
	double alfa;
	// If gama and delta are not equal to 1, include appropriate declaration
	// double gama = 1, delta = 1;
};
