#include "allocationMatrices.h"
#include <iostream>
#include <algorithm>

constexpr int POZITIVNA_BESKONACNOST = 1e9;

void inicijalizacija_pocetnih_matrica_alokacije (std::vector<std::vector<double> >& dist,
                                                std::vector<int> haboviNajbolje,
                                                const Graf& graf,
                                                std::vector<std::vector<int> >& A, 
                                                std::vector<std::vector<int> >& B) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

    A.resize(graf.brojCvorova);
    B.resize(graf.brojCvorova);
	dist.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++){
        A[i].resize(graf.brojCvorova);
        B[i].resize(graf.brojCvorova);
		dist[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), i);
            auto nadjiJuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), j);

            A[i][j] = POZITIVNA_BESKONACNOST;
            B[i][j] = POZITIVNA_BESKONACNOST;

            if (nadjiIuHabovi == std::end(haboviNajbolje) && nadjiJuHabovi == std::end(haboviNajbolje))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(haboviNajbolje) && nadjiJuHabovi != std::end(haboviNajbolje))
                dist[i][j] = graf.alfa * graf.cene[i][j];
            else
                dist[i][j] = graf.cene[i][j];

            #if 0
            // If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            else if (nadjiIuHabovi != std::end(haboviNajbolje)))
                dist[i][j] = graf.delta * graf.cene[i][j];
            else
                dist[i][j] = graf.gama * graf.cene[i][j];
            #endif
		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = graf.brojCvorova.
void popuni_matrice_alokacije (std::vector<std::vector<double> >& dist,
                            std::vector<int> haboviNajbolje,
                            const Graf& graf,
                            std::vector<std::vector<int> >& A,
                            std::vector<std::vector<int> >& B) {

	for (int brojac = 0; brojac < graf.p; brojac++) {
		for (int i = 0; i < graf.brojCvorova; i++) {
			for (int j = 0; j < graf.brojCvorova; j++) {
                auto nadjiIuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), i);
                auto nadjiJuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), j);

				if (dist[i][j] > dist[i][haboviNajbolje[brojac]] + dist[haboviNajbolje[brojac]][j]) {
					dist[i][j] = dist[i][haboviNajbolje[brojac]] + dist[haboviNajbolje[brojac]][j];

					if (A[i][haboviNajbolje[brojac]] != POZITIVNA_BESKONACNOST) {
						A[i][j] = A[i][haboviNajbolje[brojac]];
						B[i][j] = haboviNajbolje[brojac];
					} else if (A[haboviNajbolje[brojac]][j] != POZITIVNA_BESKONACNOST) {
						A[i][j] = haboviNajbolje[brojac];
						B[i][j] = A[haboviNajbolje[brojac]][j];
					} else {
						A[i][j] = haboviNajbolje[brojac];
						B[i][j] = POZITIVNA_BESKONACNOST;
					}
                }
            }
        }
    }
}