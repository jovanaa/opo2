#pragma once

#include <vector>
#include "graph.h"

void inicijalizacija_pocetnih_matrica_alokacije(std::vector<std::vector<double> >& dist,
                                                std::vector<int> haboviNajbolje,
                                                const Graf& graf,
                                                std::vector<std::vector<int> >& A, 
                                                std::vector<std::vector<int> >& B);
void popuni_matrice_alokacije(std::vector<std::vector<double> >& dist,
                            std::vector<int> haboviNajbolje,
                            const Graf& graf,
                            std::vector<std::vector<int> >& A,
                            std::vector<std::vector<int> >& B);