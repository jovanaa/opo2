#pragma once

#include <vector>
#include <fstream>
#include "graph.h"

Graf ucitajCAB(const char* nazivFajla);
Graf ucitajCAB_testing(const char* nazivFajla, std::ifstream& fajlSaTestInstancama);
Graf ucitajAP(const char* nazivFajla);
Graf ucitajURAND(const char* nazivFajla);