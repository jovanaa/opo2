#include "SA.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include "modFloydWarshallFuncs.h"
#include "typesOfInstances.h"
#include "inputFunctions.h"
#include "timer.h"
#include "allocationMatrices.h"
#include "outputFunctions.h"

std::vector<int> inicijalizuj(const Graf& graf) {
	std::vector<int> habovi;
	std::vector<double> maxTrosakZaDatiCvor;
	maxTrosakZaDatiCvor.resize(graf.brojCvorova);
	habovi.resize(graf.p);
	std::vector<std::pair<double, int> > parIndeksVr;
	
	// For each node h find maximum cost of transport, considering all costs form h to every other node.
	// That is, find max(chj), where j goes through the set of nodes.
	for (int i = 0; i < graf.brojCvorova; i++) {
			maxTrosakZaDatiCvor[i] = graf.cene[i][0];
		for (int j = 1; j < graf.brojCvorova; j++) {
			maxTrosakZaDatiCvor[i] = std::max(maxTrosakZaDatiCvor[i], graf.cene[i][j]);
		}
		
		parIndeksVr.push_back(std::make_pair(maxTrosakZaDatiCvor[i], i));
	}
	
	// Sort the array of the maximum costs in ascending order. Keep track of indexes of nodes.
	std::sort(parIndeksVr.begin(), parIndeksVr.end());
	
	// Take graf.p nodes with the minimal previously found maximal costs. Set those graf.p nodes to be hubs.
	for (int i = 0; i < graf.p; i++)
		habovi[i] = parIndeksVr[i].second;

	return habovi;
}

double vrednost_resenja(const Graf& graf, std::vector<int>& habovi) {
	std::vector<std::vector<double> > dist;

	inicijalizacija_pocetne_matrice(graf, habovi, dist);
	pronadji_najkrace_puteve(graf, habovi, dist);
	double max = pronadji_max_cenu_najkracih_puteva(graf, dist);

	return max;
}

// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj(const Graf& graf, std::vector<int>& habovi) {
	int k = std::rand() % graf.p;
	int j = habovi[k];
	
	int i;
	do {
		i = std::rand() % graf.brojCvorova;
	} while (std::find(habovi.begin(), habovi.end(), i) != habovi.end());
	habovi[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(std::vector<int>& habovi, int j, int k) {
	habovi[k] = j;
}

// Algorithm 1. See science paper.
double simulirano_kaljenje_algoritam1(std::ostream& izlaz,
							const Graf& graf,
                            std::vector<int>& habovi,
                            std::vector<int>& haboviNajbolje,
                            const ParametriSA& parametri,
                            Stoperica& stoperica) {
	double trenutnaVrednost = vrednost_resenja(graf, habovi);	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;					// f* = f(H)
	int iteracija = 0;
	
	haboviNajbolje.resize(graf.p); 

	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (protekloVremeSA < parametri.MAX_VREME_SA) {
		std::pair<int, int> par = invertuj(graf, habovi);		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = vrednost_resenja(graf, habovi);	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)					// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;					// f(H) = f(H')
		else {
			double zavesa = 1.0 / std::pow(iteracija, 0.5);
			#if 0	// ADJUST: Choose which value for "zavesa" will be used.
			double zavesa = log(2) / log(1 + iteracija);
			#endif

			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else 
				vrati(habovi, j, k);
			
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			haboviNajbolje = habovi;			

			izlaz << "\tTrenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
			double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
			izlaz << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	}
	
	return najboljaVrednost;
}

// Algorithm 2. See science paper.
double simulirano_kaljenje_algoritam2(std::ostream& izlaz,
							const Graf& graf,
                            std::vector<int>& habovi,
                            std::vector<int>& haboviNajbolje,
                            const ParametriSA& parametri,
                            Stoperica& stoperica) {
	double trenutnaVrednost =   vrednost_resenja(graf, habovi);	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;					// f* = f(H)
	
	haboviNajbolje.resize(graf.p);

	double temperatura = parametri.pocetnaTemperatura;
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (temperatura > 0) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < parametri.maxPonavljanjaNaIstojTemp) {
			std::pair<int, int> par = invertuj(graf, habovi);		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
			
			double novaVrednost = vrednost_resenja(graf, habovi);	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)					// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;					// f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else 
					vrati(habovi, j, k);
				
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				haboviNajbolje = habovi;			
				
				izlaz << "\tTrenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
				double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
				izlaz << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
			}
			
			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
		}
		
		temperatura *= parametri.parametarHladjenja;
	}

	return najboljaVrednost;
}

// Algorithm 1. See science paper. For hybrid.
double simulirano_kaljenje_algoritam1_hibrid(std::ostream& izlaz,
												const Graf& graf,
												Jedinka& jedinka,
												const ParametriSA& parametriSA,
												const ParametriGA& parametriGA,
												Stoperica& stoperica) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;					// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;					// f* = f(H)
	int iteracija = 0;

	double protekloVreme = stoperica.izracunaj_proteklo_vreme();
	while (iteracija++ < parametriSA.MAX_ITERACIJA_SA && protekloVreme < parametriGA.MAX_VREME_GA) {
		std::pair<int, int> par = invertuj(graf, jedinka.kod);	// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = vrednost_resenja(graf, jedinka.kod);	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)					// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;					// f(H) = f(H')
		else {
			double zavesa = 1.0 / std::pow(iteracija, 0.5);
			#if 0	// ADJUST: Choose which value for "zavesa" will be used.
			double zavesa = log(2) / log(1 + iteracija);
			#endif

			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else 
				vrati(jedinka.kod, j, k);
			
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			najboljaJedinka.kod = jedinka.kod;
			najboljaJedinka.fitnes = najboljaVrednost;			

			izlaz << "\tTrenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
			double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
			izlaz << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVreme = stoperica.izracunaj_proteklo_vreme();
	}
	
	jedinka = najboljaJedinka;

	return najboljaVrednost;
}

// Algorithm 2. See science paper. For hybrid.
double simulirano_kaljenje_algoritam2_hibrid(std::ostream& izlaz,
												const Graf& graf,
												Jedinka& jedinka,
												const ParametriSA& parametriSA,
												const ParametriGA& parametriGA,
												Stoperica& stoperica) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;						// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;						// f* = f(H)

	double temperatura = parametriSA.pocetnaTemperatura;
	double protekloVreme = stoperica.izracunaj_proteklo_vreme();
	while (temperatura > 0 && protekloVreme < parametriGA.MAX_VREME_GA) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < parametriSA.maxPonavljanjaNaIstojTemp && protekloVreme < parametriGA.MAX_VREME_GA) {
			std::pair<int, int> par = invertuj(graf, jedinka.kod);		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
			
			double novaVrednost = vrednost_resenja(graf, jedinka.kod);	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)						// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;						// f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else 
					vrati(jedinka.kod, j, k);
				
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				najboljaJedinka.kod = jedinka.kod;
				najboljaJedinka.fitnes = najboljaVrednost;				
				
				izlaz << "\tTrenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost;
				double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
				izlaz << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
			}
			
			brojPonavljanjaNaIstojTemp++;
			protekloVreme = stoperica.izracunaj_proteklo_vreme();
		}
		
		temperatura *= parametriSA.parametarHladjenja;
	}

	jedinka = najboljaJedinka;

	return najboljaVrednost;
}