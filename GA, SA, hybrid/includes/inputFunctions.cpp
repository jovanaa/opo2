#include "inputFunctions.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <chrono>

Graf ucitajCAB(const char* nazivFajla) {
	Graf graf;

	std::cout << "\nUnesite broj cvorova: ";
	std::cin >> graf.brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> graf.p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> graf.alfa;
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	graf.cene.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		graf.cene[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> graf.cene[i][j];
		}
	}
		
	ulaz.close();

	return graf;
}

// For testing.
Graf ucitajCAB_testing(const char* nazivFajla, std::ifstream& fajlSaTestInstancama) {
	Graf graf;

	fajlSaTestInstancama >> graf.brojCvorova;
	fajlSaTestInstancama >> graf.p;
	fajlSaTestInstancama >> graf.alfa;
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
	
	graf.cene.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		graf.cene[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> graf.cene[i][j];
		}
	}
		
	ulaz.close();

	return graf;
}

Graf ucitajAP(const char* nazivFajla) {
	Graf graf;
	graf.alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> graf.brojCvorova;
	
	std::vector<double> x, y;
	x.reserve(graf.brojCvorova);
	y.reserve(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	graf.cene.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		graf.cene[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++) {
			graf.cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	//Ignore next graf.brojCvorova lines.
	for (int i = 0; i < graf.brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> graf.p;
		
	ulaz.close();

	return graf;
}

Graf ucitajURAND(const char* nazivFajla) {
	Graf graf;
	graf.alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> graf.brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(graf.brojCvorova);
	y.reserve(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	graf.cene.resize(graf.brojCvorova);
	for (int i = 0; i < graf.brojCvorova; i++) {
		graf.cene[i].resize(graf.brojCvorova);
		for (int j = 0; j < graf.brojCvorova; j++) {
			graf.cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next graf.brojCvorova lines.
	for (int i = 0; i < graf.brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> graf.p;
		
	ulaz.close();
	
	return graf;
}