#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include "includes/SA.h"
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

#if 0	// Can be used for benchmarking.
class Stoperica {
public:
	std::ofstream& m_outputFajlSaVremenima;

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> m_pocetniTrenutak;

public:
	Stoperica(std::ofstream& outputFajlSaVremenima) : m_outputFajlSaVremenima(outputFajlSaVremenima) {
		m_pocetniTrenutak = std::chrono::high_resolution_clock::now();
	}

	~Stoperica() {
		zaustavi();
	}

	void zaustavi() {
		auto krajnjiTrenutak = std::chrono::high_resolution_clock::now();

		auto pocetak = std::chrono::time_point_cast<std::chrono::microseconds>(m_pocetniTrenutak).time_since_epoch().count();
		auto kraj = std::chrono::time_point_cast<std::chrono::microseconds>(krajnjiTrenutak).time_since_epoch().count();

		auto trajanje = kraj - pocetak;
		double ms = trajanje * 0.001;

		// std::cout << "Vreme: " << trajanje << " us, " << ms << " ms \n";
		m_outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\t\t\tVreme: " << trajanje << " us, " << ms << " ms";
	}
};
#endif

void test(std::ifstream& fajlSaTestInstancama,
			ParametriSA& parametri,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom);

int main() {
	ParametriSA parametri;
	// ADJUST: Adjust parameter values.
	parametri.MAX_VREME_SA = 1000;	// Time in milliseconds.
	#if 0	// Set the following parameters only if Algorithm 2 is used for simulirano_kaljenje().
	parametri.pocetnaTemperatura = 43482.6461;
	parametri.parametarHladjenja = 0.4709;	
	parametri.maxPonavljanjaNaIstojTemp = 4097;
	#endif

	srand(time(NULL));

	// ADJUST: Adjust file locations.
	std::ifstream fajlSaTestInstancama("testInstances-small.txt");
	std::ofstream outputFajlSaVremenima("rezultati-SA-small.txt");
	std::ofstream outputFajlSaHabovimaIMatricamaAlok("rezultati-SA-saMatricama-small.txt");

	// ADJUST: Adjust number of runs for each instance.
	int brojPokretanjaNadJednomInstancom = 10;
	
	//ADJUST: Adjust fourth argument of function test().
	// CAB
	test(fajlSaTestInstancama,
			parametri,
			CAB /* tipInstance */,
			12 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// AP
	test(fajlSaTestInstancama,
			parametri,
			AP /* tipInstance */,
			8 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// URAND
	test(fajlSaTestInstancama,
			parametri,
			URAND /* tipInstance */,
			0 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);

	fajlSaTestInstancama.close();
  	outputFajlSaVremenima.close();
	outputFajlSaHabovimaIMatricamaAlok.close();

	return 0;
}

void test(std::ifstream& fajlSaTestInstancama,
			ParametriSA& parametri,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom) {
	if (!brojInstanci)
		return;

	// Input and output comment from file "testInstance.txt"
	std::string instancaKomentar;
	getline(fajlSaTestInstancama, instancaKomentar);
	outputFajlSaVremenima << instancaKomentar << std::endl;

	int obradjenoInstanci = 0;
	while (obradjenoInstanci < brojInstanci) {
		std::string instanca;
		getline(fajlSaTestInstancama, instanca);

		Graf graf;	// Includes: brojCvorova, cene (cost matrix), p, alfa

		if (tipInstance == CAB) {
			graf = ucitajCAB_testing(instanca.c_str(), fajlSaTestInstancama);
			fajlSaTestInstancama.ignore(); // Ignore new line character; only needed with CAB instances...see input file
		} else if (tipInstance == AP)
			graf = ucitajAP(instanca.c_str());
		else if (tipInstance == URAND)
			graf = ucitajURAND(instanca.c_str());
		else {
			std::cout << "Nekorektan unos.\n";
			return;
		}

		// ADJUST: Use in case different time criteria is needed for instances of different sizes.
		#if 0
		if (graf.brojCvorova < 1000)
			parametri.MAX_VREME_SA = 900000; // Time in milliseconds.
		else
			parametri.MAX_VREME_SA = 1800000; // Time in milliseconds.
		#endif

		outputFajlSaVremenima << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		outputFajlSaHabovimaIMatricamaAlok << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		for (int i = 0; i < brojPokretanjaNadJednomInstancom; i++) {
			outputFajlSaVremenima << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;
			outputFajlSaHabovimaIMatricamaAlok << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;

			std::vector<int> habovi; // Set of hubs. In the GA this is used as the code of an individual.
			std::vector<int> haboviNajbolje;
			
			Stoperica meriUkupnoVreme;	// Start timer.

			habovi = inicijalizuj(graf);
			// ADJUST: Choose between simulirano_kaljenje_algoritam1() and simulirano_kaljenje_algoritam2().
			// In case simulirano_kaljenje_algoritam1() is used, there are 2 options for function "zavesa". See SA.cpp.
			double vrednostResenja = simulirano_kaljenje_algoritam1(outputFajlSaVremenima,
																	graf,
																	habovi,
																	haboviNajbolje,
																	parametri,
																	meriUkupnoVreme);

			double protekloVremePreAB = meriUkupnoVreme.izracunaj_proteklo_vreme();	// Stop timer;
			outputFajlSaVremenima << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms";

			// Calculate allocation matrices.
			std::vector<std::vector<double> > dist;
			std::vector<std::vector<int> > A; // First allocation matrix.
			std::vector<std::vector<int> > B; // Second allocation matrix.
			inicijalizacija_pocetnih_matrica_alokacije(dist, 
													haboviNajbolje,
													graf,
													A,
													B);
			popuni_matrice_alokacije(dist,
									haboviNajbolje,
									graf,
									A,
									B);

			double ukupnoVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\nUkupno vreme racunajuci rad sa AB: " << ukupnoVreme << " ms\n\n";

			
			ispisi_vrednost_resenja_i_habove(outputFajlSaHabovimaIMatricamaAlok, vrednostResenja, haboviNajbolje, graf.p);
			ispisi_matrice_alokacije(outputFajlSaHabovimaIMatricamaAlok, A , B, graf.brojCvorova);
			outputFajlSaHabovimaIMatricamaAlok << std::endl;
		}

		obradjenoInstanci++;
	}

	fajlSaTestInstancama.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Skip empty line in input file
}
