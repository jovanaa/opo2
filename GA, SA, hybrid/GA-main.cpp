#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include "includes/GA.h"
#include "includes/graph.h"
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

int main(int argc, char** argv) {

	ParametriGA parametri;
	// ADJUST: Adjust parameter values.
	parametri.velicinaPopulacije = 103;
    parametri.brojElitnihJedinki = 33;
    parametri.velicinaTurnira = 2;
    parametri.verovatnocaMutacije = 0.6641;
    parametri.MAX_VREME_GA = 1000;	// Time in milliseconds.
	
	srand(time(NULL));

	unsigned instance;
	std::cout << "\nUnesite broj 1 ako se program pokrece nad CAB instancama, broj 2 ukoliko se pokrece nad AP instancama ili broj 3 ukoliko su u pitanju URAND instance: ";
	std::cin >> instance;
	std::cout << "\n";
	static_cast<tipInstance>(instance);

	Graf graf;	// Includes: brojCvorova, cene (cost matrix), p, alfa

	// Read instance file.
	if (instance == CAB) 
		graf = ucitajCAB(argv[1]);
	else if (instance == AP)
		graf = ucitajAP(argv[1]);
	else if (instance == URAND)
		graf = ucitajURAND(argv[1]);
	else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

	Stoperica stopericaGA;	// Start timer.
	
	std::vector<Jedinka> populacija;
	std::vector<Jedinka> novaPopulacija;
	populacija.reserve(parametri.velicinaPopulacije);
	novaPopulacija.reserve(parametri.velicinaPopulacije);
	for (int i = 0; i < parametri.velicinaPopulacije; i++) {
		populacija.emplace_back(Jedinka(graf));
		novaPopulacija.emplace_back(Jedinka(graf));
	}
	std::sort(populacija.begin(), populacija.end(), uporedi);

	double najboljaVrednost = populacija[0].fitnes;
	std::cout << "\n\tTrenutna najbojla vrednost: " << najboljaVrednost;
	double protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "\n\tVreme: " << protekloVremeGA << " ms \n";

	int neparanBrojJedinkiZaUkrstanjeIMutaciju = (parametri.velicinaPopulacije - parametri.brojElitnihJedinki) % 2;
	while (protekloVremeGA < parametri.MAX_VREME_GA) {

		for (int i = 0; i < parametri.brojElitnihJedinki; i++)
			novaPopulacija[i] = populacija[i];
		

		// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
		if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
			for (int i = parametri.brojElitnihJedinki; i < parametri.velicinaPopulacije-1; i+=2) {
				int i1 = selekcija(parametri, populacija);
				int i2 = selekcija(parametri, populacija);
				
				// ADJUST: Choose which crossover type will be called.
				// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
				jednopoziciono_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(graf, parametri, novaPopulacija[i]);
				mutacija(graf, parametri, novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
			}

			mutacija(graf, parametri, novaPopulacija[parametri.velicinaPopulacije-1]);
			novaPopulacija[parametri.velicinaPopulacije-1].fitnes = novaPopulacija[parametri.velicinaPopulacije-1].fitnes_funkcija(graf);
		} else {	// Even number of individuals on which genetic operators should be applied.
			for (int i = parametri.brojElitnihJedinki; i < parametri.velicinaPopulacije; i+=2) {
				int i1 = selekcija(parametri, populacija);
				int i2 = selekcija(parametri, populacija);
				
				// ADJUST: Choose which crossover type will be called.
				// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
				jednopoziciono_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(graf, parametri, novaPopulacija[i]);
				mutacija(graf, parametri, novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
				
			}
		}
		
		populacija = novaPopulacija;
		std::sort(populacija.begin(), populacija.end(), uporedi);

		if (populacija[0].fitnes < najboljaVrednost) {
			najboljaVrednost = populacija[0].fitnes;
			
			std::cout << "Trenutna najbojla vrednost: " << najboljaVrednost << "\t";
			double vremeTrazenjaNoveNajboljeVrednosti = stopericaGA.izracunaj_proteklo_vreme();
			std::cout << "Vreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	}

	// Output time spent, NOT including time for calculating allocation matrices.
	double protekloVremePreAB = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms \n";

	// Calculate allocation matrices.
	std::vector<std::vector<double> > dist;
	std::vector<std::vector<int> > A; // First allocation matrix.
	std::vector<std::vector<int> > B; // Second allocation matrix.
	inicijalizacija_pocetnih_matrica_alokacije(dist, 
											populacija[0].kod /* habovi */,
											graf,
											A,
											B);
	popuni_matrice_alokacije(dist,
							populacija[0].kod /* habovi */,
							graf,
							A,
							B);

	// Output time spent, including time for calculating allocation matrices.
	double protekloVreme = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme: " << protekloVreme << " ms \n";

	// Output results.
	std::ofstream outputFajl("AP102-rezultati.txt");	// ADJUST: Adjust output file name.
	ispisi_vrednost_resenja_i_habove(outputFajl,
									populacija[0].fitnes /* vrednostResenja */,
									populacija[0].kod /* habovi */,
									graf.p);
	ispisi_matrice_alokacije(outputFajl, A , B, graf.brojCvorova);
	outputFajl.close();

	return 0;
}
