#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include "includes/GA.h"
#include "includes/SA.h"
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

void test(std::ifstream& fajlSaTestInstancama,
			ParametriGA& parametriGA,
			const ParametriSA& parametriSA,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom);

int main(int argc, char** argv) {
	ParametriGA parametriGA;
	// ADJUST: Adjust parameter values.
	parametriGA.velicinaPopulacije = 141;
    parametriGA.brojElitnihJedinki = 119;
    parametriGA.velicinaTurnira = 6;
    parametriGA.verovatnocaMutacije = 0.5611;
    parametriGA.MAX_VREME_GA = 1000;	// Time in milliseconds.

	ParametriSA parametriSA;
	// ADJUST: Adjust parameter values.
	parametriSA.MAX_ITERACIJA_SA = 467;
	#if 0	// Set the following parameters only if simulirano_kaljenje_algoritam2_hibrid() is used.
	parametriSA.pocetnaTemperatura = 43482.6461;
	parametriSA.parametarHladjenja = 0.4709;	
	parametriSA.maxPonavljanjaNaIstojTemp = 4097;
	#endif

	srand(time(NULL));

	// ADJUST: Adjust file locations.
	std::ifstream fajlSaTestInstancama("testInstances-small.txt");
	std::ofstream outputFajlSaVremenima("rezultati-hybrid-small.txt");
	std::ofstream outputFajlSaHabovimaIMatricamaAlok("rezultati-hybrid-saMatricama-small.txt");

	// ADJUST: Adjust number of runs for each instance.
	int brojPokretanjaNadJednomInstancom = 10;

	//ADJUST: Adjust fourth argument of function test().
	// CAB
	test(fajlSaTestInstancama,
			parametriGA,
			parametriSA,
			CAB /* tipInstance */,
			12 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// AP
	test(fajlSaTestInstancama,
			parametriGA,
			parametriSA,
			AP /* tipInstance */,
			8 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// URAND
	test(fajlSaTestInstancama,
			parametriGA,
			parametriSA,
			URAND /* tipInstance */,
			0 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);

	fajlSaTestInstancama.close();
  	outputFajlSaVremenima.close();
	outputFajlSaHabovimaIMatricamaAlok.close();

	return 0;
}

void test(std::ifstream& fajlSaTestInstancama,
			ParametriGA& parametriGA,
			const ParametriSA& parametriSA,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom) {
	if (!brojInstanci)
		return;

	// Input and output comment from file "testInstance.txt"
	std::string instancaKomentar;
	getline(fajlSaTestInstancama, instancaKomentar);
	outputFajlSaVremenima << instancaKomentar << std::endl;

	int obradjenoInstanci = 0;
	while (obradjenoInstanci < brojInstanci) {
		std::string instanca;
		getline(fajlSaTestInstancama, instanca);

		Graf graf;	// Includes: brojCvorova, cene (cost matrix), p, alfa

		if (tipInstance == CAB) {
			graf = ucitajCAB_testing(instanca.c_str(), fajlSaTestInstancama);
			fajlSaTestInstancama.ignore(); // Ignore new line character; only needed with CAB instances...see input file
		} else if (tipInstance == AP)
			graf = ucitajAP(instanca.c_str());
		else if (tipInstance == URAND)
			graf = ucitajURAND(instanca.c_str());
		else {
			std::cout << "Nekorektan unos.\n";
			return;
		}

		// ADJUST: Use in case different time criteria is needed for instances of different sizes.
		#if 0
		if (graf.brojCvorova < 1000)
			parametriGA.MAX_VREME_GA = 900000; // Time in milliseconds.
		else
			parametriGA.MAX_VREME_GA = 1800000; // Time in milliseconds.
		#endif

		outputFajlSaVremenima << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		outputFajlSaHabovimaIMatricamaAlok << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		for (int i = 0; i < brojPokretanjaNadJednomInstancom; i++) {
			outputFajlSaVremenima << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;
			outputFajlSaHabovimaIMatricamaAlok << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;

			Stoperica meriUkupnoVreme;	// Start timer.
			
			// Generate initial generation.
			std::vector<Jedinka> populacija;
			std::vector<Jedinka> novaPopulacija;
			populacija.reserve(parametriGA.velicinaPopulacije);
			novaPopulacija.reserve(parametriGA.velicinaPopulacije);
			for (int i = 0; i < parametriGA.velicinaPopulacije; i++) {
				populacija.emplace_back(Jedinka(graf));
				novaPopulacija.emplace_back(Jedinka(graf));
			}
			std::sort(populacija.begin(), populacija.end(), uporedi);

			double najboljaVrednost = populacija[0].fitnes;
			outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbojla vrednost: " << najboljaVrednost;
			double protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\n\tVreme: " << protekloVremeGA << " ms \n";

			// ADJUST: Choose between simulirano_kaljenje_algoritam1_hibrid() and simulirano_kaljenje_algoritam2_hibrid().
			// In case simulirano_kaljenje_algoritam1_hibrid() is used, there are 2 options for function "zavesa". See SA.cpp.
			// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost.
			najboljaVrednost = simulirano_kaljenje_algoritam1_hibrid(outputFajlSaVremenima,
																	graf,
																	populacija[0] /* jedinka */,
																	parametriSA,
																	parametriGA,
																	meriUkupnoVreme /* stoperica */);			

			int neparanBrojJedinkiZaUkrstanjeIMutaciju = (parametriGA.velicinaPopulacije - parametriGA.brojElitnihJedinki) % 2;
			while (protekloVremeGA < parametriGA.MAX_VREME_GA) {

				for (int i = 0; i < parametriGA.brojElitnihJedinki; i++)
					novaPopulacija[i] = populacija[i];
				
				// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
				if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
					for (int i = parametriGA.brojElitnihJedinki; i < parametriGA.velicinaPopulacije-1; i+=2) {
						int i1 = selekcija(parametriGA, populacija);
						int i2 = selekcija(parametriGA, populacija);
						
						// ADJUST: Choose which crossover type will be called.
						// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
						uniformno_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
						
						mutacija(graf, parametriGA, novaPopulacija[i]);
						mutacija(graf, parametriGA, novaPopulacija[i + 1]);
						
						novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
						novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
					}

					mutacija(graf,
							parametriGA,
							novaPopulacija[parametriGA.velicinaPopulacije-1]);
					novaPopulacija[parametriGA.velicinaPopulacije-1].fitnes = novaPopulacija[parametriGA.velicinaPopulacije-1].fitnes_funkcija(graf);
				} else {	// Even number of individuals on which genetic operators should be applied.
					for (int i = parametriGA.brojElitnihJedinki; i < parametriGA.velicinaPopulacije; i+=2) {
						int i1 = selekcija(parametriGA, populacija);
						int i2 = selekcija(parametriGA, populacija);
						
						// ADJUST: Choose which crossover type will be called.
						// Options to choose from: jednopoziciono_ukrstanje, dvopoziciono_ukrstanje, uniformno_ukrstanje.
						uniformno_ukrstanje(graf.p, populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
						
						mutacija(graf, parametriGA, novaPopulacija[i]);
						mutacija(graf, parametriGA, novaPopulacija[i + 1]);
						
						novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija(graf);
						novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija(graf);
						
					}
				}
				populacija = novaPopulacija;
				std::sort(populacija.begin(), populacija.end(), uporedi);

				if (populacija[0].fitnes < najboljaVrednost) {
					najboljaVrednost = populacija[0].fitnes;
					
					outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbojla vrednost: " << najboljaVrednost;
					outputFajlSaVremenima << "\n\tVreme: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms \n";
				}

				// ADJUST: Choose between simulirano_kaljenje_algoritam1_hibrid() and simulirano_kaljenje_algoritam2_hibrid().
				// In case simulirano_kaljenje_algoritam1_hibrid() is used, there are 2 options for function "zavesa". See SA.cpp.
				// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost.
				najboljaVrednost = simulirano_kaljenje_algoritam1_hibrid(outputFajlSaVremenima,
														graf,
														populacija[0] /* jedinka */,
														parametriSA,
														parametriGA,
														meriUkupnoVreme /* stoperica */);

				protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
			}

			outputFajlSaVremenima << "Ukupno vreme pre racunanja A i B matrice: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms";

			// Calculate allocation matrices.
			std::vector<std::vector<double> > dist;
			std::vector<std::vector<int> > A; // First allocation matrix.
			std::vector<std::vector<int> > B; // Second allocation matrix.
			inicijalizacija_pocetnih_matrica_alokacije(dist, 
													populacija[0].kod /* habovi */,
													graf,
													A,
													B);
			popuni_matrice_alokacije(dist,
									populacija[0].kod /* habovi */,
									graf,
									A,
									B);

			outputFajlSaVremenima << "\nUkupno vreme racunajuci rad sa AB: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms\n\n";
			
			ispisi_vrednost_resenja_i_habove(outputFajlSaHabovimaIMatricamaAlok,
											populacija[0].fitnes /* vrednostResenja */,
											populacija[0].kod /* habovi */,
											graf.p);
			ispisi_matrice_alokacije(outputFajlSaHabovimaIMatricamaAlok, A , B, graf.brojCvorova);

			outputFajlSaHabovimaIMatricamaAlok << std::endl;
		}

		obradjenoInstanci++;
	}

	fajlSaTestInstancama.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Skip empty line in input file
}
