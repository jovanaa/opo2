#include <time.h>
#include <math.h>
#include <ilcplex/cplex.h>

//F-ja za ispis greske
void error (const char* message, CPXENVptr env = NULL, CPXLPptr lp = NULL) {
	printf ("%s. \n", message);

	if(env != NULL && lp != NULL)
		CPXfreeprob(env, &lp);

	if(env != NULL)
		CPXcloseCPLEX(&env);

	exit(0);
}

//Alociranje nizova za uslove
void allocate (double** rhs, char** sense, int** matbeg, int** matind, double** matval, int n, CPXENVptr env, CPXLPptr lp) {
	*rhs = (double*)malloc(sizeof(double));
	if (*rhs == NULL) 
		error("Greska pri alokaciji memorije", env, lp);
	*matbeg = (int*)malloc(sizeof(int));
	if (*matbeg == NULL) 
		error("Greska pri alokaciji memorije", env, lp);
	*sense = (char*)malloc(sizeof(char));
	if (*sense == NULL) 
		error("Greska pri alokaciji memorije", env, lp);
	*matind = (int*)malloc(n * sizeof(int));
	if (*matind == NULL) 
		error("Greska pri alokaciji memorije", env, lp);
	*matval = (double*)malloc(n * sizeof(double));
	if (*matval == NULL) 
		error("Greska pri alokaciji memorije", env, lp);
}

//Oslobadjanje nizova za uslove
void free (double** rhs, char** sense, int** matbeg, int** matind, double** matval) {
	free(*rhs);
	free(*matbeg);
	free(*sense);
	free(*matind);
	free(*matval);
}

int main(int argc, char** argv) {
	//Pocetno vreme
	double startTime = clock();
	//"nbNodes" je kardinalnost skupa N iz rada, "nb" je pomocna promenljiva 
	//dok je uloga "instance" promenljive da prati nad kojim instancama se radi (bitno zbog formata ulaznih podataka)
	int p, nbNodes, nb, instance;
	float alfa, gama, delta;
	//cost predstavlja matricu [cij] iz rada
	double** cost;

	//Ucitavanje ulaznih podataka
	FILE* in = fopen(argv[1], "r");

	printf("\nUnesite broj 1 ako se program pokrece nad CAB instancama ili broj 2 ukoliko se pokrece nad AP instancama: ");
	scanf("%d", &instance);
	printf("\n");

	//Ucitavanje ulaznih podataka ukoliko su u pitanju CAB instance
	if (instance == 1) {
		printf("Unesite broj cvorova: ");
		scanf("%d", &nbNodes);

		printf("Unesite broj habova: ");
		scanf("%d", &p);

		printf("Unesite vrednost za alfa: ");
		scanf("%f", &alfa);

		cost = (double**)malloc(nbNodes * sizeof(double*));
		double baci;
		for (int i = 0; i < nbNodes; i++) {
			cost[i] = (double*)malloc(nbNodes * sizeof(double));
			for (int j = 0; j < nbNodes; j++) {
				fscanf(in, "%lf", &baci);
				fscanf(in, "%lf", &baci);
				fscanf(in, "%lf", &baci);
				fscanf(in, "%lf", &cost[i][j]);
			}
		}
	//Ucitavanje ulaznih podataka ukoliko su u pitanju AP instance
	} else if (instance == 2) {
		alfa = 0.75;

		fscanf(in, "%d", &nbNodes);
		double x[nbNodes], y[nbNodes];
		for (int i = 0; i < nbNodes; i++) {
			fscanf(in, "%lf %lf", &x[i], &y[i]);
		}

		cost = (double**)malloc(nbNodes * sizeof(double*));
		for (int i = 0; i < nbNodes; i++) {
			cost[i] = (double*)malloc(nbNodes * sizeof(double));
			for (int j = 0; j < nbNodes; j++) { 
				cost[i][j] = sqrt(pow(x[i] - x[j], 2) + pow(y[i] - y[j], 2));
			}
		}

		double baci;
		for (int i = 0; i < nbNodes; i++) {
			for (int j = 0; j < nbNodes; j++) { 
				fscanf(in, "%lf", &baci);
			}
		}

		fscanf(in, "%d", &p);
	} else {
		printf("\n");
		error("Nekorektan unos");
	}

	gama = 1;
	delta = 1;

	fclose(in);

	printf("\n");
	
	//cmax
	double cmax = 0.0;
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			if (cost[i][j] > cmax)
				cmax = cost[i][j];
		}
	}

	//Pomocna promenljiva
	int nbNodesKv = (int)pow(nbNodes,2);

	// Otvaranje CPLEX-a i podesavanje pocetnih parametara
	int status;
	CPXENVptr env = CPXopenCPLEX(&status);
	if (status)
		error("Ne moze se otvoriti CPLEX");

	//Kreiranje problema
	CPXLPptr lp = CPXcreateprob(env, &status, "UMApHCP");
	if (status)
		error("Greska pri kreiranju problema", env);

	//Minimizacija
	status = CPXchgobjsen(env, lp, CPX_MIN);
	if (status)
		error("Definisanje problema kao problema tipa minizmizacije nije uspelo", env, lp);

	//Podesavanje promenljivih
	//+ 1 zbog r; + nbNodes zbog Zk
	int nbU = pow(nbNodes, 3);
	int nbV = pow(nbNodes, 3);
	int nbVariables = nbU + nbV + nbNodes + 1;

	double* coeffs = (double*)malloc(nbVariables * sizeof(double));
	double* lowerBound = (double*)malloc(nbVariables * sizeof(double));
	double* upperBound = (double*)malloc(nbVariables * sizeof(double));
	char* type = (char*)malloc(nbVariables * sizeof(char));

	//Uijk
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			for (int k = 0; k < nbNodes; k++) {
				coeffs[i * nbNodesKv + j * nbNodes + k] = 0.0;
				lowerBound[i * nbNodesKv + j * nbNodes + k] = 0.0;
				upperBound[i * nbNodesKv + j * nbNodes + k] = 1.0;
				type[i * nbNodesKv + j * nbNodes + k] = 'B';
			}
		}
	}

	//Vijl
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			for (int l = 0; l < nbNodes; l++) {
				coeffs[i * nbNodesKv + j * nbNodes + l + nbU] = 0.0;
				lowerBound[i * nbNodesKv + j * nbNodes + l + nbU] = 0.0;
				upperBound[i * nbNodesKv + j * nbNodes + l + nbU] = 1.0;
				type[i * nbNodesKv + j * nbNodes + l + nbU] = 'B';
			}
		}
	}

	// Zk
	for (int k = nbU + nbV; k < nbVariables - 1; k++) {
		coeffs[k] = 0.0;
		lowerBound[k] = 0.0;
		upperBound[k] = 1.0;
		type[k] = 'B';
	}

	//r
	coeffs[nbVariables - 1] = 1;
	lowerBound[nbVariables - 1] = -CPX_INFBOUND;
	upperBound[nbVariables - 1] = CPX_INFBOUND;
	type[nbVariables - 1] = 'C';

	//Dodavanje promenljivih
	status = CPXnewcols(env, lp, nbVariables, coeffs, lowerBound, upperBound, type, NULL);
	if (status)
		error("Ne mogu se dodati nove kolone", env, lp);

	double* rhs;
	char* sense;
	int* matbeg;
	int* matind;
	double* matval;

	//Uslov (9)
	allocate (&rhs, &sense, &matbeg, &matind, &matval, nbNodes, env, lp);

	rhs[0] = p * 1.0;
	sense[0] = 'E';
	matbeg[0] = 0;

	for (int k = 0; k < nbNodes; k++) {
		matind[k] = nbU + nbV + k;
		matval[k] = 1.0;
	}

	nb = nbNodes;
	status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
	if (status)
		error("Ne mogu se dodati novi redovi", env, lp);

	free (&rhs, &sense, &matbeg, &matind, &matval);

	//Uslov (10)
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			for (int k = 0; k < nbNodes; k++) {
				allocate(&rhs, &sense, &matbeg, &matind, &matval, 2, env, lp);

				rhs[0] = 0.0;
				sense[0] = 'L';
				matbeg[0] = 0;

				matind[0] = i * nbNodesKv + j * nbNodes + k;
				matval[0] = 1.0;

				matind[1] = nbU + nbV + k;
				matval[1] = -1.0;

				nb = 2;
				status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
				if (status)
					error("Ne mogu se dodati novi redovi", env, lp);

				free(&rhs, &sense, &matbeg, &matind, &matval);
			}
		}
	}

	//Uslov (11)
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			for (int l = 0; l < nbNodes; l++) {
				allocate(&rhs, &sense, &matbeg, &matind, &matval, 2, env, lp);

				rhs[0] = 0.0;
				sense[0] = 'L';
				matbeg[0] = 0;

				matind[0] = i * nbNodesKv + j * nbNodes + l + nbU;
				matval[0] = 1.0;

				matind[1] = nbU + nbV + l;
				matval[1] = -1.0;

				nb = 2;
				status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
				if (status)
					error("Ne mogu se dodati novi redovi", env, lp);

				free(&rhs, &sense, &matbeg, &matind, &matval);
			}
		}
	}


	//Uslov (12)
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			allocate(&rhs, &sense, &matbeg, &matind, &matval, nbNodes, env, lp);

			rhs[0] = 1.0;
			sense[0] = 'E';
			matbeg[0] = 0;

			for (int k = 0; k < nbNodes; k++) {
				matind[k] = i * nbNodesKv + j * nbNodes + k;
				matval[k] = 1.0;
			}

			nb = nbNodes;
			status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
			if (status)
				error("Ne mogu se dodati novi redovi", env, lp);

			free(&rhs, &sense, &matbeg, &matind, &matval);
		}
	}

	//Uslov (13)
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			allocate(&rhs, &sense, &matbeg, &matind, &matval, nbNodes, env, lp);

			rhs[0] = 1.0;
			sense[0] = 'E';
			matbeg[0] = 0;

			for (int l = 0; l < nbNodes; l++) {
				matind[l] = i * nbNodesKv + j * nbNodes + l + nbU;
				matval[l] = 1.0;
			}

			nb = nbNodes;
			status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
			if (status)
				error("Ne mogu se dodati novi redovi", env, lp);

			free(&rhs, &sense, &matbeg, &matind, &matval);
		}
	}


	//Uslov (14)
	for (int i = 0; i < nbNodes; i++) {
		for (int j = 0; j < nbNodes; j++) {
			if(j <= i){
				for (int t = 0; t < nbNodes; t++) {
					allocate(&rhs, &sense, &matbeg, &matind, &matval, nbNodes + nbNodes + 1, env, lp);

					rhs[0] = alfa * cmax;
					sense[0] = 'L';
					matbeg[0] = 0;

					for (int k = 0; k < nbNodes; k++) {
						matind[k] = i * nbNodesKv + j * nbNodes + k;
						matval[k] = gama * cost[i][k] + alfa * cost[k][t];
					}

					for (int l = 0; l < nbNodes; l++) {
						if (l == t) {
							matind[l + nbNodes] = i * nbNodesKv + j * nbNodes + l + nbU;
							matval[l + nbNodes] = delta * cost[l][j] + alfa * cmax;
						} else {
							matind[l + nbNodes] = i * nbNodesKv + j * nbNodes + l + nbU;
							matval[l + nbNodes] = delta * cost[l][j];
						}
					}

					matind[nbNodes * 2] = nbVariables - 1;
					matval[nbNodes * 2] = -1.0;

					nb = nbNodes + nbNodes + 1;
					status = CPXaddrows(env, lp, 0, 1, nb, rhs, sense, matbeg, matind, matval, NULL, NULL);
					if (status)
						error("Ne mogu se dodati novi redovi", env, lp);

					free(&rhs, &sense, &matbeg, &matind, &matval);
				}
			}
		}
	}

	//Maksimalno vreme izvrsavanja
	status = CPXsetdblparam(env, CPX_PARAM_TILIM, 7200.00);
	if (status)
		error("Uspostavljanje maksimalnog vremena izvrsavanja programa nije uspelo", env, lp);


	// Generisanje rezultata i zatvaranje CPLEX-a

	//Podesavanje tipa problema
	status = CPXchgprobtype(env, lp, CPXPROB_MILP);
	if (status)
		error("Podesavanje tipa problema nije uspelo", env, lp);

	//Optimizacija
	status = CPXmipopt(env, lp);
	if (status)
		error("Optimizacija nije uspela", env, lp);

	//Resenje i njegova vrednost
	double objval;
	status = CPXgetobjval(env, lp, &objval);
	if (status)
		error("Resenje se ne moze dobiti. Proverite da li ste odabrali dobre instance", env, lp);

	//Broj cvorova, iteracija i gap
	int nodes = CPXgetnodecnt(env, lp);
	int iter = CPXgetmipitcnt(env, lp);
	double gap;
	status = CPXgetmiprelgap(env, lp, &gap);
	if (status)
		error("Greska pri zahtevanju gap vrednosti", env, lp);

	//Koje cvorove treba uzeti za habove
	double* solution = (double*) malloc(nbNodes *sizeof(double));
	status = CPXgetx(env, lp, solution, nbU + nbV, nbVariables - 2);
	if (status)
		error("Greska pri odredjivanju vektora resenja", env, lp);

	printf("Za habove uzeti cvorove (indeksiranje pocinje od nule): ");
	for (int k = 0; k < nbNodes; k++) {
		if (solution[k] == 1)
			printf(" %d", k);
	}

	printf(".\n");

	//Vreme izvrsavanja
	double stopTime = clock();
	double timeInSeconds = (stopTime - startTime) / CLOCKS_PER_SEC;

	//Ispis rezultata
	printf("Resenje: %.2lf.\n", objval);
	printf("Vreme u sekundama: %.3lf\n", timeInSeconds);
	printf("Broj cvorova: %d\n", nodes);
	printf("Broj iteracija: %d\n", iter);
	printf("Gap: %.2lf.\n", gap);

	//Zatvaranje problema
	if (env != NULL && lp != NULL) {
		status = CPXfreeprob(env, &lp);
		if (status)
			error("Greska pri zatvaranju problema", env, lp);
	}

	//Zatvaranje CPLEX-a
	if (env != NULL && lp != NULL) {
		status = CPXcloseCPLEX(&env);
		if (status)
			error("Greska pri zatvaranju CPLEX-a", env, lp);
	}
		

	return 0;
}
